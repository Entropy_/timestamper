package main

import (
   
    "os"
    "fmt"
    "time"
    "io/ioutil"
)

func main() {
    exists := false
    _, err := os.Stat("time.stamp")
	if err == nil {
		exists = true
	}
    if exists {
	    numdir, err := ioutil.ReadDir("./")
	    if err != nil {
			panic(err)
	    }
	    file, err := os.Create("time.stamp"+fmt.Sprint(len(numdir)))
	if err != nil {
		panic(err)
	}
	defer file.Close()
	val := time.UTC.String()
	file.WriteString(val)
    }else {
	    file, err := os.Create("time.stamp")
	    if err != nil {
        		fmt.Println("stamp already exists")
		}
		defer file.Close()
		file.WriteString(time.UTC.String())
	}
}
